#!/usr/bin/bash
asciidoctor-revealjs -v -r asciidoctor-diagram -r asciidoctor-html5s -D public presentation.adoc
cp -r images public/
cp -r reveal.js public/
cp -r plugins public/
